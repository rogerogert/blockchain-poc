#Prova de Conceito (Proof of Ownership)

Esse projeto é uma pequena demonstração de como funciona os contratos inteligentes do Ehereum. É uma prova de conceito de Proof of Ownership utilizando a blockchain e desenvolvemos com o objetivo de apresentar para a Certisign.

#Instalação

Para criar uma blockchain de testes da Ethereum é necessário instalar o seguinte pacote via `npm`. É necessário possuir o Node.JS (>= v6.9.1).

```Bash
npm install -g ethereumjs-testrpc
```

É necessário também possuir os seguintes pacotes para compilar o contrato escrito em Solidity e migrar para a Blockchain do Ethereum rodando no testrpc.

```Bash
npm install --global --production windows-build-tools
npm install -g truffle
```

#Deploy do Contrato na Ethereum

Antes de compilar o contrato utilizando o truffle, é necessário abrir uma janela de comando e iniciar um ambiente virtual de testes da Ethereum utilizando o comando `testrpc`. Esse comando irá criar dez carteiras com ethereum disponível para executar os contratos (gas). 

Iniciando o Power Shell (não consegui usando o cmd) na pasta OwnershipContract, compile o contrato a partir do comando `truffle compile`.

Em seguida, execute o `truffle migrate` para deploy do contrato na Ethereum rodando no testrpc. A porta padrão utilizada é a 8545. O migrate irá retornar a chave pública do contrato minerado na linha com o nome do contrato (Proof). Exemplo: `Proof: 0x920fecf28db3257fa85727988a265a3363ae3021`. Copie o endereço retornado.

#Utilizando o Contrato

O Web App disponível no diretório Ownership Site é uma página que roda na porta 8080 que é capaz de utilizar as funçoes do contrato. Antes de iniciar o site, é necessário indicar a chave pública do contrato no arquivo app.js. Isso é feito na seguinte linha:

```Bash
var proof = proofContract.at("0x920fecf28db3257fa85727988a265a3363ae3021");
```
Feito isso, para iniciar o Web App, basta abrir o prompt de comando no diretório OwnershipSite e executar ´node app.js´ e iniciar o site no endereço http://localhost:8080.








